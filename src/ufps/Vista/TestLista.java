/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class TestLista {
    
    public static void main(String[] args) {
        ListaS<String> nombres=new ListaS();
        nombres.insertarInicio("marco");
        nombres.insertarInicio("gederson");
        nombres.insertarInicio("diana");
        nombres.insertarInicio("carlos");
        System.out.println("Cardinalidad:"+nombres.getCardinalidad());
        System.out.println(nombres.toString());
        System.out.println("Verificar si diana está en la lista:"+nombres.esta("diana"));
        System.out.println("Verificar si PEPE está en la lista:"+nombres.esta("PEPE"));
        
        //Creando una lista de Personas:
        ListaS<Persona> personas=new ListaS();
        personas.insertarInicio(new Persona(1,"madarme"));
        personas.insertarInicio(new Persona(3,"gederson"));
        personas.insertarInicio(new Persona(4,"diana"));
        System.out.println(personas);
        
        Persona x=new Persona(3, "gederson");
        System.out.println("Verificamos si:"+x.toString()+", está en la lista:"+personas.esta(x));
        
        
    }
}
